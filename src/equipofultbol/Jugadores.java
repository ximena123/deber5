/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package equipofultbol;
import java.util.*;
/**
 *
 * @author Ximena Puchaicela
 */

public abstract class Jugadores{
    int valoracion=0 ;
    public abstract int valoracion();
}
class Equipo extends Jugadores{
    Scanner teclado = new Scanner(System.in);
    boolean aux;
    int jugador;
    int suma;
    public int goles;
    public int valoracionGoles;
    public Equipo (){}
    public Equipo(int goles){
        this.goles=goles;
    }
    public void agregar(int arreglo[],int contador){
    do{
            aux = true;
            System.out.println("Ingrese: "); jugador = teclado.nextInt();
            for (int i = 0; i < contador; i++) {
                if(jugador==arreglo[i])
                    aux = false;
            }
            if (aux) {
                arreglo[contador]= jugador;
                contador++;
                aux = false;
            }else{
                System.out.println("Error");
                aux = true;
            } 
       }while(aux);
    }
    public int resultados(int arreglo[]){
        for (int i = 0; i < arreglo.length; i++) {
            suma +=arreglo[i];
        }
        return suma;
    }
    public void jugadas(int jugadores[],int arreglo[],int numero,int gol){
        int aux=0;
        for (int i = 0; i < arreglo.length; i++) {
            if(numero == jugadores[i]){
                aux =i;
                arreglo[aux]+= gol;
            }
        }
    }
    public void presentar(int arreglo[]){
        for (int i = 0; i < arreglo.length; i++) {
            System.out.println(arreglo[i]+" ");
        }
    }
    public int sacar(int arreglo[],int numero,int jugadores[]){
        int valor=-1;
        for (int i = 0; i < arreglo.length; i++) {
            if(numero == jugadores[i])
                valor = arreglo[i];
        }
        return valor;
    }
    
    @Override
    public  int valoracion(){
        valoracionGoles = goles*30;
        return valoracionGoles;
    }          
}
class Porteros extends Equipo{
    int atajadas;
    public Porteros(){}
    public Porteros(int atajadas, int goles) {
        super(goles);
        this.atajadas = atajadas;
    }
    @Override
    public int valoracion(){
        valoracion = super.valoracion()+atajadas*5;
        return valoracion;
    } 
    @Override
    public String toString() {
        return "la valoracion del arquero es: "+ valoracion();
    }
}
class Atacantes extends Equipo{
     int pasesExitosos;
     int balonesRecuperados;
    public Atacantes(int pasesExitosos, int balonesRecuperados, int goles) {
        super(goles);
        this.pasesExitosos = pasesExitosos;
        this.balonesRecuperados = balonesRecuperados;
    }
     @Override
    public int valoracion(){
        valoracion = super.valoracion()+(pasesExitosos*2)+(balonesRecuperados*3);
        return valoracion;
    }     
    @Override
    public String toString() {
        return "La valoracion del atacante es : " + valoracion();
    }          
}
class Defensores extends Equipo{
    int pasesExitosos;
    int balonesRecuperados;
    public Defensores(int pasesExitosos, int balonesRecuperados, int goles) {
        super(goles);
        this.pasesExitosos = pasesExitosos;
        this.balonesRecuperados = balonesRecuperados;
    }
    @Override
       public int valoracion(){
        valoracion = super.valoracion()+pasesExitosos+balonesRecuperados*4;
        return valoracion;
    } 
    @Override
    public String toString() {
        return "La valoracion del defensor es : "+ valoracion();
    }           
}
class PruebaEquipo{
    public static void main(String[] args) {  
        Scanner teclado = new Scanner (System.in);
        Equipo equipo1= new Equipo();
        int jugadores[]= new int [3];
        int opc=0,numero,gol=1;
        int goles[]= new int [3];
        int pases[]= new int [3];
        int recuperaciones[]= new int [3];
        int valoraciones[]= new int [3];
        int contador=0;
        int atajadas,gole,pase,recuperacion;
        int opcion=0;
        int acumulador=0;
        while(opc!=6){
            System.out.println("Elija una opcion:\n1.Agregar jugador\n2.Anotar Gol\n3.Dar Pase\n4.Recuperar\n5.Valoracion individual\n6.Valoracion total");opc = teclado.nextInt();
            switch(opc){
                case 1:
                    equipo1.agregar(jugadores, contador);
                    contador++;
                    break;
                case 2:
                    equipo1.presentar(jugadores);
                    System.out.println("Ingrese el numero de camiseta del jugador: ");numero= teclado.nextInt();
                    equipo1.jugadas(jugadores,goles,numero,gol);
                    break;
                case 3:
                    System.out.println("Ingrese el numero de camisera del jugador: ");numero = teclado.nextInt();
                    equipo1.jugadas(jugadores, pases, numero, gol);
                    break;
                case 4:
                    System.out.println("Ingrese el numero de camisera del jugador: ");numero = teclado.nextInt();
                    equipo1.jugadas(jugadores, recuperaciones, numero, gol);
                    break;
                case 5: 
                    gole =equipo1.resultados(goles);
                    System.out.println("Ingrese el numero de camiseta: ");numero = teclado.nextInt();
                        System.out.println("1.Atacante\n2.Defensor\n3.Portero\n4.Atras");opcion = teclado.nextInt();
                        switch(opcion){
                            case 1:
                                pase= equipo1.sacar(pases, numero, jugadores);
                                recuperacion = equipo1.sacar(recuperaciones, numero, jugadores);
                                Atacantes atacante = new Atacantes(pase,recuperacion,gole);
                               System.out.println(atacante);
                               acumulador += atacante.valoracion();
                                break;     
                            case 2:
                                pase= equipo1.sacar(pases, numero, jugadores);
                                recuperacion = equipo1.sacar(recuperaciones, numero, jugadores);
                                Defensores defensa = new Defensores(pase,recuperacion,gole);
                                System.out.println(defensa);
                                acumulador += defensa.valoracion();
                                break;
                            case 3:
                                atajadas = equipo1.sacar(goles, numero, jugadores);
                                Porteros portero = new Porteros(atajadas,gole);
                                System.out.println(portero);
                                acumulador+=portero.valoracion();
                                break;
                        }  
                        break;
                case 6:
                    System.out.println(acumulador);
            }
        }

    }
    
}
